#!/usr/bin/awk -f

/* Do this before anything else */
BEGIN {
	print "This is mawk calling awesome....";
	totalbytes=0;
	totalfiles=0;
	totaldirs=0;
}

/* Start of the main loop */
{
	totalbytes+=$5;
	if ( $1 ~ /^-/ ) {
		totalfiles+=1;
	}
	if ( $1 ~ /^d/ ) {
		totaldirs+=1;
	}
}

END {
	print "Current size of the directory is: ",totalbytes;
	print "Number of files in this directory: ",totalfiles;
	print "Number of directories in here are: ",totaldirs;
}
